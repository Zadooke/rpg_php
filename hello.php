<html> 
<head>
<meta charset="UTF-8">
<title>Hello World</title>
</head>

<body>

<?php
    function returnNumber() {
        return rand(1, 10);
    }

    function printHelloWorld() {        
        $name = returnNumber();
        echo "World ".$name;
        if(isset($_GET["name"]) && $_GET["name"] == "Arthur") {
            echo "<h1> Olá Arthur, bem vindo de volta. </h1>";
        }    
    }

    printHelloWorld();
?>

</body>

</html>