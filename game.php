<html>
<link rel="stylesheet" type= "text/css" href="question.php">
<head>
    <meta charset = "UTF-8">
    
    <?php
    # Init
    include_once("helper.php");
    include_once("console.php");
    include_once("mysql_connect.php");

    session_start();

    Connect();
    $conn = Session("mysql_connection");

    $questionAmount = $conn->query("select count(*) from pergunta");
    $questionAmount = $questionAmount->fetch_row();

    $questionID = rand(0, $questionAmount[0] - 1);
    //echo "Current Question: " . $questionID;

    header('Refresh: 60');

    # Pega informações
    $temp = $conn->query("select * from jogador where jogador.id = " . Session("ID"));
    $row = $temp->fetch_assoc();
    
    $curID = $row['ID'];
    $curLife = $row['Vida'];
    $curPos = $row['Posicao'];

    # Checa se alguém já venceu
    $jogo = $conn->query("select * from jogo");
    $venceu = $jogo->fetch_assoc();

    if($venceu['id_jogando'] == $curID) {   // Venceu
        $conn->query("delete from jogador where jogador.id = " . Session("ID"));
        header("Location: win.html");
    }
    else if ($venceu['id_jogando'] != -1 && $venceu['id_jogando'] != $curID){
        $conn->query("delete from jogador where jogador.id = " . Session("ID"));
        header("Location: lose.html");
    }

    function GetPlayer($id) {
        $temp = $conn->query("select * from jogador where jogador.id = " . $id);
        $row = $temp->fetch_assoc();

        return $row;
    }


    
    if(Post("button_quit") != null) {
        # Remove o Player do banco
        $conn->query("delete from jogador where jogador.id = " . Session("ID"));
        header('Location: rpg.php');
    }
    ?>
</head>
<body>
    <?php
        $pergunta = $conn->query("select * from pergunta where pergunta.ID = " . $questionID)->fetch_assoc();
        $textopergunta = $pergunta['S_Pergunta'];
        //echo $pergunta['S_Pergunta'];
     ?>

    <br>
    

    <?php

    if (isset($_SESSION["lastRespostas"]) && isset($_SESSION["lastCorrect"]))
    {
        $lastRespostas = $_SESSION["lastRespostas"];
        $lastCorrect = $_SESSION["lastCorrect"];

        for($i = 0; $i < 5; $i++)
        {
            if(isset($_POST[$i])){

                echo "Button Index: ".$i;
                echo "LastAnswer: ".$lastRespostas[$_POST[$i]];
                echo "LastCorrectAnswer: ".$lastCorrect;

                if ($lastRespostas[$_POST[$i]] == $lastCorrect)
                {
                    $newPos = $curPos + 1;
                    $conn->query("update jogador set Posicao = '{$newPos}' WHERE jogador.ID = {$curID}");
                    # Checa se venceu
                    if($newPos >= 10) {
                        $jogo = $conn->query("update jogo SET id_jogando = {$curID} where jogo.id_jogando = -1");
                    }
                }
                else
                {
                    $newLife = $curLife - 1;
                    $conn->query("update jogador set Vida = '{$newLife}' WHERE jogador.ID = {$curID}");

                    if($newLife <= 0) {
                        # Remove o Player do banco
                        $conn->query("delete from jogador where jogador.id = " . Session("ID"));
                        header('Location: lose.html');
                    }
                }
            }
        }
    }



        # Pega todas as respostas da pergunta
        $respostas = $conn->query("select * from pergunta where pergunta.ID = " . $questionID);

        $row = mysqli_fetch_assoc($respostas);

        $a = $row['correta'];
        $b = $row['b'];
        $c = $row['c'];
        $d = $row['d'];
        $e = $row['e'];

        $listaRespostas = array(
            $a,
            $b,
            $c,
            $d,
            $e
        );

        $correta = $row['correta'];

        shuffle($listaRespostas);

    $_SESSION["lastRespostas"] = $listaRespostas;
    $_SESSION["lastCorrect"] = $row['correta'];

        // for($i = 0; $i < 5; $i++) 
        // {
            // echo "
            // <form action = '#', method = 'post'>
            // $listaRespostas[$i] - 
            // <input type = 'submit', value = '$i', name = $i>
            // </form>
            // ";   
        // }


//        # Checa se o jogador escolheu a resposta certa
//        if(isset($_POST[$correta])) {
//
//            echo $_POST[$correta];
//
//            $newPos = $curPos + 1;
//            $conn->query("update jogador set Posicao = '{$newPos}' WHERE jogador.ID = {$curID}");
//
//            # Checa se venceu
//            if($newPos >= 10) {
//                $jogo = $conn->query("update jogo SET id_jogando = {$curID} where jogo.id_jogando = -1");
//            }
//        }
//        else {
//            $newLife = $curLife - 1;
//            $conn->query("update jogador set Vida = '{$newLife}' WHERE jogador.ID = {$curID}");
//
//            if($newLife <= 0) {
//                # Remove o Player do banco
//                $conn->query("delete from jogador where jogador.id = " . Session("ID"));
//                header('Location: lose.html');
//            }
//        }


        # Checa Outro jogador
        $otherPlayer = null;

        
        if(Session("ID") == 1) {
            $temp = $conn->query("select * from jogador where jogador.id = 2");
            $otherPlayer = $temp->fetch_assoc();
        }
        else if(Session("ID") == 2) {
            $temp = $conn->query("select * from jogador where jogador.id = 1");
            $otherPlayer = $temp->fetch_assoc();
        }

        $otherID = $otherPlayer['ID'];
        $otherLife = $otherPlayer['Vida'];
        $otherPosition = $otherPlayer['Posicao'];


        DebugLog("Player " . $curID . "\n"
        . "Life Points: " . $curLife . "\n"
        . "Position: " . $curPos );
        
        DebugLog2("Player " . $otherID . "\n"
        . "Life Points: " . $otherLife . "\n"
        ."Position: " . $otherPosition );
    ?>

        <div class="question_mark">
        </div>  
   <div class="container">
        
        
        
        <div class="question">
            
            <p>
                <?php echo $textopergunta?>
            </p>



        </div>

        <div class="box1">
            <div class="answer1">
                <?php
                echo $listaRespostas[0];
                ?>
            </div>
            <div class = "botao1">
            <?php  echo "            
                <form action = '#', method = 'post'>
                <input type = 'submit', value = '0', name = 0>
                </form>";?>
            </div>
        </div>
        <div class="box2">
            <div class="answer2">
                <?php
                echo $listaRespostas[1];
                ?>
            </div>
            <div class = "botao1">
            <?php  echo "            
                <form action = '#', method = 'post'>
                <input type = 'submit', value = '1', name = 1>
                </form>";?>
            </div>
        </div>
        <div class="box3">
            <div class="answer3">
                <?php
                echo $listaRespostas[2];
                ?>
            </div>
            <div class = "botao1">
            <?php  echo "            
                <form action = '#', method = 'post'>
                <input type = 'submit', value = '2', name = 2>
                </form>";?>
            </div>
        </div>

        <div class="box4">
            <div class="answer4">
                <?php
                echo $listaRespostas[3];
                ?>
            </div>
            <div class = "botao1">
            <?php  echo "            
                <form action = '#', method = 'post'>
                <input type = 'submit', value = '3', name = 3>
                </form>";?>
            </div>
        </div>

        <div class="box5">
            <div class="answer5">
                <?php
                echo $listaRespostas[4];
                ?>
            </div>
            <div class = "botao1">
            <?php  echo "            
                <form action = '#', method = 'post'>
                <input type = 'submit', value = '4', name = 4>
                </form>";?>
            </div>
        </div>

        <div class = "botaoquit">
            <?php  echo "            
                <form action = '#', method = 'post'>
                <input type = 'submit', value = Quit, name = button_quit>
                </form>";?>
        </div>

    
    <?php     # Quit
    //FormButton("#", "post", "Quit", "button_quit");?>
</body>
</html>
