<?php header("Content-type: text/css; charset: UTF-8"); 

include "Animations.css";
include "Resources.php";


switch($theme)
{
    case 0: // Lake
    $bgtheme = $lakeurl;
    break;
    
    case 1: // Mountain
    $bgtheme = $mountainurl;
    break;
}
?>

body{
    margin: 0 px;
    background-image: url("<?php echo $bgtheme; ?>");
    background-size: 100%;
    background-repeat: no-repeat;    
    text-align: center;
}


@keyframes rotateanimation{
    from {
        opacity: 0;
      }
      to {
        opacity: 1;
      }
    }

.container{
    width: 500px;
    height: auto;
    background-image: url("<?php echo $blueboard; ?>");
    background-size: 100%;
    background-repeat: no-repeat;
    margin-top: 0;
    margin: 0 auto;
}
.question_mark{    
    width: 150px;
    height: 150px;
    background-image: url("<?php echo $question_mark; ?>");
    background-size: 100%;
    background-repeat: no-repeat; 
    margin-top: 3%;
    margin: 0 auto;
}
.question{    
    text-align: center;
    color: rgb(255, 255, 255);
    font-family: Arial;
    font-size: 28px;
    font-weight: bold;
    padding-top: 10px;      
}
.box1{
    display:flex;
    justify-content : center;
    margin:0 auto;
}
.answer1{    
    color: rgb(255, 255, 255);
    font-family: Arial;
    font-size: 20px;
    font-weight: bold;       
    float : left;    
}
.botao1{    
    float:left; 
}
.box2{
    display:flex;
    justify-content : center;
    margin:0 auto;
}
.answer2{    
    color: rgb(255, 255, 255);
    font-family: Arial;
    font-size: 20px;
    font-weight: bold;
    float : left;    
}
.botao2{    
    float:left; 
}
.box3{
    display:flex;
    justify-content : center;
    margin:0 auto;
}
.answer3{    
    color: rgb(255, 255, 255);
    font-family: Arial;
    font-size: 20px;
    font-weight: bold;
    float : left;    
}
.botao3{    
    float:left; 
}
.box4{
    display:flex;
    justify-content : center;
    margin:0 auto;
}
.answer4{    
    color: rgb(255, 255, 255);
    font-family: Arial;
    font-size: 20px;
    font-weight: bold;
    float : left;    
}

.botao4{    
    float:left; 
}
.box5{
    display:flex;
    justify-content : center;
    margin:0 auto;
}
.answer5{    
    color: rgb(255, 255, 255);
    font-family: Arial;
    font-size: 20px;
    font-weight: bold;
    float : left;    
}

.botao5{    
    float:left; 
}