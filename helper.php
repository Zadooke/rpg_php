<?php
    /**
    * Cria um Botão dentro do seu próprio Form
    * @param ActionType $action
    * @param Method $method
    * @param Value $value
    * @param Name $name 
    */
    function FormButton($action, $method, $value, $name) {
        echo 
        "<form action = " . $action . " , method = ".$method.">
            <input type = 'submit', value = " . $value . " , name = " . $name . ">
        </form>
        ";
    }

    function Session($index) {
        if(!isset($_SESSION[$index]))
            return null;
        
        $result = $_SESSION[$index];

        if($result == null) {
            exit ("No session member defined: " .$index);
        }
        else {
            return $result;
        }
    }

    function SessionPut($key, $value) {
        $_SESSION[$key] = $value;
    }

    function PostGet($key) {
        $result = $_POST[$key];

        if(!isset($result))
            exit("Could not find POST object with key " . $key);

        return $result;
    }

    function PostCheck($key, $value = true) {
        $result = $_POST[$key];
        
        if(!isset($result))
            exit("Could not find POST object with key " . $key);
        
        return true;
    }

    function Post($key) {
       if(isset($_POST[$key]))
            return $_POST[$key];
        else
            return null;
    }
?>