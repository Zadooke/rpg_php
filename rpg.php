<html>
<head>
<meta charset = "UTF-8">
<link rel="stylesheet" type= "text/css" href="rpg.css">

</head>
<body>
    <!-- Init -->
    <?php
       function Init() {
            # Inclui o SQL
            include_once("mysql_connect.php");
            include_once("helper.php");
            include_once("console.php");

            # Inicia a Sessão
            session_start();
        }

        Init();

        # Debug, mata a sessão
        echo "
        <div class = 'username'>
            <form action = '#', method = 'post'>
                <input type = 'submit', value = 'Reset Session', name = 'session_destroy'>
            </form>      
        <div>
        ";

        # Debug, conecta a database
        echo "
        Player Name:
        <div class = 'username'>
            <form action = '#', method = 'post'>           
                <input type = 'text', value = '', name = 'player_name'>            
                <input type = 'submit', value = 'Connect to Database', name = 'connect_database'>            
            </form>   
        </div>   
        ";

        # Vai para o Map Builder
        echo "
        Map Builder: 
        <div class = 'username'>
            <form action = 'QuestionInserterLayout.php', method = 'post'>           
                <input type = 'submit', value = 'Map Builder', name = 'map_builder'>            
            </form>   
        </div> 
        ";

        // if(Post("connect_database") != null) {
        //     Connect("localhost","jogo");
        // }

        // if(Post("map_builder") != null) {
        //     header('Location: QuestionInserterLayout.php');
        // }

        if(Post("connect_database") != null) {
            if(Post("player_name") == "") {
                echo "Por favor, cheque os valores inseridos.";
            }
            else {
                Connect();

                $conn = Session("mysql_connection");
                
                // Pega o ID atual
                $lastID = $conn->query("select id from jogador order by id desc");
                $row = $lastID->fetch_assoc();

                $id = $row['id'] + 1;
                
                SessionPut("ID", $id);

                $_SESSION["ID"] = $id;

                // Checa se já não existe esse jogador jogando
                $name = Post("player_name");

                $players = $conn->query("select nome from jogador where nome = '{$name}'");
                
                if($players) { // O Result set encontrou algo, não é nulo
                    $row = $players->fetch_assoc();
                    
                    if($row['nome'] == $name){
                        exit("Já existe um jogador com esse nome jogando.");
                    }
                }
            
                // Insere
                InsertInto($id.",10,'".Post("player_name")."',0", "jogador");
                echo "Inseriu com sucesso o jogador " . Post("player_name") . " na database.";

                DebugLog("You are player " . Session("ID"));

                //header( 'Location: game.php' );
            }
        }

        if(isset($_POST["session_destroy"])) {
            Connect();
            $conn = Session("mysql_connection");

            # Limpa o Banco
            $conn->query("delete from jogador where id != -1");
            $conn->query("update jogo set id_jogando = -1"); 

            echo "Session has ended.";
            session_destroy();
            
            Init();
        }

        if(Post("connect_database") != null) {
            # Botão Play
            echo "
            <form action = '#', method = 'post'>
                <input type = 'submit', value = 'Play', name = 'play'>
            </form>      
            ";            
        }

        if(Post("play") != null) {
            header( 'Location: game.php' );

            // # Checa se a variável no servidor de jogador atual é igual a -1
            // $conn = Session("mysql_connection");

            // $index = $conn->query("select * from 'jogo'")->fetch_assoc();
            // if($index['id_jogando'] == -1) {
            //     # Seta o jogador atual como jogando
            //     $ID = Session("ID");
            //     $conn->query("UPDATE 'jogo' SET 'id_jogando' = '{$ID}' WHERE 'jogo'.'id_jogando' = -1");
            // }
        }

        // # Vai para o Level Builder
        // FormButton("game.php", "post", "Play", "map_builder");
    ?>
</body>