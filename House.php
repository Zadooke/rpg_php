<?php header("Content-type: text/css; charset: UTF-8"); 

include "Animations.css";
include "Resources.php";

include_once("helper.php");
include_once("console.php");
include_once("mysql_connect.php");

Connect();

$question = $conn->query("select count(*) from pergunta");


$b = false;

// numero da casa
$numcasa = 3;
$numcasax = ($numcasa % 6) * -200;
$numcasay = floor($numcasa/6) * -200;

switch($theme)
{
    case 0: // Lake
    $bgtheme = $lakeurl;
    $floortheme = $grassurl;    
    break;
    
    case 1: // Mountain
    $bgtheme = $mountainurl;
    $floortheme = $rockurl; 
    break;
}
?>

body{ 
    margin: 0 px;
    background-image: url("<?php echo $bgtheme; ?>");
    background-size: 100%;
    background-repeat: no-repeat;    
    text-align: center;
}
.themebar{
    width: 500px;
    height: 100px;
    background-image: url("<?php echo $floortheme; ?>");
    position : relative;
    margin-top:  18%;  
    margin-left: 30%;   
    animation:grow_to_normal_size  1.0s;  
}

.ground{
    width: 100%;
    height: 26%;
    background-image: url("<?php echo $floortheme; ?>");
    margin-top:  10%;  
}

.player{
    width: 20%;
    height:40%;
    background-image: url("<?php echo $player_img; ?>");
    background-size: 100%;
    background-color: green;
    margin-top: -600px; 
    margin-left: 40%;
}

.numcasa{
    width: 200px;
    height: 200px;
    border-radius : 500px;
    border: 2px solid #73AD21;
    //padding-left: 50px;
    background: url("<?php echo $numrurl; ?>")  <?php echo $numcasax ?>px <?php echo $numcasay ?>px;
    background-size : 600%;
    margin-top:  10%;      
    margin-left: 45%;
}

