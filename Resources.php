<?php header("Content-type: text/css; charset: UTF-8"); 
// tema da casa
$theme = 0;

//back-ground images
$mountainurl = "bg/mountain.jpg";
$lakeurl = "bg/lake.jpg";

// ground images
$grassurl = "ground/grass.png";
$rockurl = "ground/rock.jpg";

//misc images
$numrurl = "misc/number.png";
$player_img = "misc/player.png";
$blueboard = "misc/blueboard.png";
$question_mark = "misc/question_mark.png";


//theme images
$C_theme = "misc/C.png";
$Cplusplus_theme = "misc/Cplusplus.png";
$Csharp_theme = "misc/Csharp.png";
$Java_theme = "misc/Java.png";
$PHP_theme = "misc/PHP.png";
$Lua_theme = "misc/LUA.png";


$bgtheme =$lakeurl;
$floortheme =$grassurl;
?>


/*Filter styles*/
.greencolor { rgba(0,0,255,1); }
.saturate { filter: saturate(3); }
.grayscale { filter: grayscale(100%); }
.contrast { filter: contrast(160%); }
.brightness { filter: brightness(0.25); }
.blur { filter: blur(3px); }
.invert { filter: invert(100%); }
.sepia { filter: sepia(100%); }
.huerotate { filter: hue-rotate(180deg); }
.rss.opacity { filter: opacity(50%); }